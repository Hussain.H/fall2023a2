package vehicules;

public class Bicycle{

    private String manufacturer;
    private int numberGears;
    private double maxSpeed;

    public Bicycle(String manufacturer, int numberGears, double maxSpeed){
        this.manufacturer = manufacturer;
        this.numberGears = numberGears;
        this.maxSpeed = maxSpeed;
    }

    public String getManufacturer() {
        return manufacturer;
    }
    public double getMaxSpeed() {
        return maxSpeed;
    }
    public int getNumberGears() {
        return numberGears;
    }
   public String toString(){
    return "Manufacturer: " + this.manufacturer + ", Number of Gears: " + this.numberGears 
    + ", Max Speed: " + this.maxSpeed;
   }
}
