package application;

import java.util.function.BiConsumer;

import vehicules.Bicycle;

public class BikeStore {
    

    public static void main(String[] args) {
       // vehicules.Bicycle vb = new vehicules.bicycle;
         
        Bicycle[] bicycles = new Bicycle[4]; 
        bicycles[0] = new Bicycle("Toyota",5,200);
        bicycles[1] = new Bicycle("Honda",4,180);
        bicycles[2] = new Bicycle("Harley",6,300);
        bicycles[3] = new Bicycle("Yamaha",4,160);

        for(Bicycle motorbikes : bicycles){
            System.out.println(motorbikes + " ");
        }
    }
}
